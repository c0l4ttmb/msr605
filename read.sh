cleanup ()
{
kill -s SIGTERM $!
exit 0
}

trap cleanup SIGINT SIGTERM

dir_name=$(dirname $(realpath $0))
cd $dir_name

while [ 1 ]
do
    sudo ./msr605 8 8 8 1 r &
    wait $!
done

