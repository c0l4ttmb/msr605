import signal
import subprocess

import PySimpleGUI as sg
import os

from samba.dcerpc.dcerpc import response

from stoppable_thread import Stoppable_Thread

"""
Tabs from: https://csveda.com/creating-tabbed-interface-using-pysimplegui/
Copy/paste menu : https://stackoverflow.com/questions/67859058/why-my-python-pysimplegui-window-is-not-right-clickable-so-i-can-paste-from-my

"""

COMMAND_READ = ["msr605", "8", "8", "8", "1", "r"]
COMMAND_WRITE = ["msr605", "8", "8", "8", "1", "w"]  # Concat here the code to write

INITIAL_SIZE = (500, 220)
MLINE_KEY = '-MLINE-'  # Used for copy paste
COPY_BTN_TXT = 'Copy!'

# Read tab ui
READ_BTN_TXT_DEFAULT = 'Read'
READ_BTN_TXT_STOP = 'Stop'
READ_BTN_KEY = 'btn_read'

# Read results input
READ_RESULT_KEY = 'read_button'
READ_RESULT_DEFAULT = 'Press the button to read'
READ_RESULT_WAITING = "Waiting scan.."

# Read results
READ_RESULT_OK = "ReadResultOk"
READ_RESULT_CHECKER = '[Track 2]: '
READ_RESULT_CHECKER_SIZE = 'size: 28[Track 2]: '

# Read errors
READ_ERROR = "READ_ERROR"
READ_ERROR_BAD_SUDO = "bad_sudo"
READ_ERROR_BAD_READING = "bad_reading"

# Write tab
WRITE_BTN_KEY = 'write_button'
WRITE_BTN_DEFAULT = 'Write'
WRITE_BTN_WRITING = 'Stop'
WRITE_RESULT_OK = 'WRITE OK!'
WRITE_INFO_TEXT_KEY = 'write_info_text'

# Write errors
WRITE_ERROR = "WRITE_ERROR"
WRITE_ERROR_BAD_INPUT = "bad_input"
WRITE_RESULT_FAILURE = 'MSR605 Error'


# Commmon errors
COMMON_ERRORS = "COMMON_ERRORS"
COMMON_ERROR_404 = "not_found"
COMMON_ERROR_UNKNOWN = "unknown_error"



right_click_menu = ['', ['Copy', 'Paste', 'Select All', 'Cut']]

sg.theme('Dark Purple 6')

# define layout
layout_read = [
    [sg.Button(READ_BTN_TXT_DEFAULT, key=READ_BTN_KEY)],
    [sg.InputText(READ_RESULT_DEFAULT, size=(80, 6),
                  text_color="Black",
                  use_readonly_for_disable=True,
                  disabled=True,
                  key=READ_RESULT_KEY)],
    [sg.Button(COPY_BTN_TXT, tooltip='Copy to the clipboard', )]
]
layout_write = [
    [sg.Text('Paste here:', )],
    [sg.Multiline('', key=MLINE_KEY, size=(60, 5), right_click_menu=right_click_menu)],
    [sg.Text("", key=WRITE_INFO_TEXT_KEY)],
    [sg.Button(WRITE_BTN_DEFAULT, key=WRITE_BTN_KEY)]
]

# Define Layout with Tabs
tabgrp = [
    [sg.TabGroup([
        [sg.Tab('Read', layout_read, title_color='Red', border_width=10,
                tooltip='Read from msr605', element_justification='center', expand_y=True, expand_x=True),
         sg.Tab('Write', layout_write, title_color='Blue', expand_y=True, expand_x=True,
                element_justification='center')
         ]
    ], tab_location='centertop',
        border_width=5, size=INITIAL_SIZE, expand_y=True, expand_x=True, )
    ]]

reading_thread: Stoppable_Thread = None
reading_process = None
writing_thread: Stoppable_Thread = None
writing_process = None

# Define Window
main_window = sg.Window("☠️MSR605 - Linux ☠️", tabgrp,
                        size=INITIAL_SIZE, resizable=True,
                        finalize=True, element_justification='c', )

mline: sg.Multiline = main_window[MLINE_KEY]
btn_read: sg.Button = main_window[READ_BTN_KEY]
btn_write: sg.Button = main_window[WRITE_BTN_KEY]
txt_read_result: sg.InputText = main_window[READ_RESULT_KEY]
txt_write_result: sg.Text = main_window[WRITE_INFO_TEXT_KEY]


def is_connected(output: str):
    return "Unable to open connection to device" not in output

#################
# READING
#################

# Run msr605 on read mode
def start_reading(window):
    global reading_process
    global reading_thread

    command = COMMAND_READ
    try:
        with subprocess.Popen(command,
                              stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                              stderr=subprocess.PIPE, preexec_fn=os.setsid) as p:
            reading_process = p
            try:
                out, err = p.communicate()

                result = ""
                for line in out.decode('utf-8').split("\n"):
                    print(line)
                    if READ_RESULT_CHECKER in line:
                        result = line
                        # break better to print all the result

                print("ERR:", err)

                if result != "":
                    if (READ_RESULT_CHECKER_SIZE in result):
                        window.write_event_value(READ_RESULT_OK, result.replace(READ_RESULT_CHECKER_SIZE, ""))
                    else:
                        window.write_event_value(READ_ERROR, "Bad lecture")
                # todo: not used anymore
                elif 'incorrect password attempt' in str(err):
                    window.write_event_value(READ_ERROR, READ_ERROR_BAD_SUDO)
                elif not is_connected(str(out)):
                    window.write_event_value(COMMON_ERRORS, COMMON_ERROR_404)
                elif err.__len__() > 0:
                    window.write_event_value(COMMON_ERRORS, COMMON_ERROR_UNKNOWN)
                else:
                    window.write_event_value(READ_ERROR, str(err))

            except subprocess.TimeoutExpired:
                p.kill()

    except Exception as e:
        window.write_event_value(READ_ERROR, str(e))

    reading_process = None
    reading_thread = None

def start_reading_thread(window):
    try:
        global reading_thread
        reading_thread = Stoppable_Thread(target=start_reading, args=(window,), daemon=True)
        reading_thread.start()
    except Exception as e:
        window.write_event_value(READ_ERROR, str(e))


def stop_reading():
    global reading_thread
    global reading_process
    if reading_process is not None:
        os.killpg(os.getpgid(reading_process.pid), signal.SIGINT)
        reading_process = None
    if reading_thread is not None:
        reading_thread.terminate()
    set_reading_ui(False)

# UI state for reading
def set_reading_ui(disabled: bool):
    if not disabled and txt_read_result.get() == READ_RESULT_WAITING: txt_read_result.update(READ_RESULT_DEFAULT)
    else: txt_read_result.update(READ_RESULT_WAITING)
    btn_read.update(READ_BTN_TXT_STOP) if disabled else btn_read.update(READ_BTN_TXT_DEFAULT)
    btn_write.update(disabled=disabled)
    mline.update(disabled=disabled)

#################
# Writing
#################

def start_writing(window, data = ""):
    global writing_process
    global writing_thread

    try:
        int(data, 16)
    except:
        window.write_event_value(WRITE_ERROR, WRITE_ERROR_BAD_INPUT)
        writing_thread = None
        return

    command = COMMAND_WRITE + [data]
    writes = 0
    try:
        while True:
            with subprocess.Popen(command,
                                  stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                                  stderr=subprocess.PIPE, preexec_fn=os.setsid) as p:
                writing_process = p

                out, err = p.communicate()
                print("ERR:", err)
                print("OUT:", out)

                result = ""
                for line in out.decode('utf-8').split("\n"):
                    print(line)
                    if WRITE_RESULT_FAILURE in line:
                        result = line
                    elif WRITE_RESULT_OK in line:
                        result = line
                if result != "":
                    print(result)
                    if (WRITE_RESULT_OK in line):
                        writes += 1
                        window.write_event_value(WRITE_RESULT_OK, writes)
                    elif not is_connected(str(out)):
                        window.write_event_value(COMMON_ERRORS, COMMON_ERROR_404)
                    else:
                        window.write_event_value(WRITE_ERROR, result)
                elif err.__len__() > 0 and "double free or corruption" in err.decode('utf-8'):
                    print("double free or corruption")
                    writes += 1
                    window.write_event_value(WRITE_RESULT_OK, writes)
                elif err.__len__() > 0:
                    window.write_event_value(COMMON_ERRORS, COMMON_ERROR_UNKNOWN)
                else:
                    window.write_event_value(COMMON_ERRORS, str(err))

    except Exception as e:
        window.write_event_value(READ_ERROR, str(e))

    writing_process = None
    writing_thread = None


def start_writing_thread(window, data: str):
    try:
        global writing_thread
        writing_thread = Stoppable_Thread(target=start_writing, args=(window, data), daemon=True)
        writing_thread.start()
    except Exception as e:
        window.write_event_value(READ_ERROR, str(e))



def stop_writing():
    global writing_thread
    global writing_process
    if writing_process is not None:
        print("Killing writing process")
        os.killpg(os.getpgid(writing_process.pid), signal.SIGINT)
        writing_process = None
    if writing_thread is not None:
        writing_thread.terminate()
    set_writing_ui(False)

def set_writing_ui(writing: bool):
    mline.update(disabled=writing)
    if writing:
        btn_write.update(WRITE_BTN_WRITING)
    else:
        btn_write.update(WRITE_BTN_DEFAULT)
        txt_write_result.update(' ')
    btn_read.update(disabled=writing)

while True:
    event, values = main_window.read()  # type: (str, dict)
    print(event, values)
    if event in (sg.WIN_CLOSED, 'Exit'):
        break
    # Copy Paste
    if event == 'Select All':
        mline.Widget.selection_clear()
        mline.Widget.tag_add('sel', '1.0', 'end')
    elif event == 'Copy':
        try:
            text = mline.Widget.selection_get()
            main_window.TKroot.clipboard_clear()
        except:
            print('Nothing selected')
    elif event == 'Paste':
        mline.Widget.insert(sg.tk.INSERT, main_window.TKroot.clipboard_get())
    elif event == 'Cut':
        try:
            text = mline.Widget.selection_get()
            main_window.TKroot.clipboard_clear()
            main_window.TKroot.clipboard_append(text)
            mline.update('')
        except:
            print('Nothing selected')
    # Copy button
    elif event == COPY_BTN_TXT:
        main_window.TKroot.clipboard_clear()
        main_window.TKroot.clipboard_append(values[READ_RESULT_KEY])
    # READ
    elif event == READ_BTN_KEY:
        if btn_read.get_text() == READ_BTN_TXT_STOP:
            stop_reading()
        else:
            set_reading_ui(True)
            start_reading_thread(main_window)
    elif event == READ_RESULT_OK:
        sg.popup('Read Ok!',
                 title="Hey you dumb!")
        stop_reading()
        txt_read_result.update(values[READ_RESULT_OK])
    # Write
    elif event == WRITE_RESULT_OK:
        txt_write_result.update("Write done: " + str(values[WRITE_RESULT_OK]))
    elif event == WRITE_BTN_KEY:
        if btn_write.get_text() == WRITE_BTN_DEFAULT:
            set_writing_ui(True)
            start_writing_thread(main_window, mline.get())
        else:
            set_writing_ui(False)
            stop_writing()
    # Raising common errors
    elif event == COMMON_ERRORS:
        if values[COMMON_ERRORS] == COMMON_ERROR_404:
            sg.popup('Msr605 not found', 'Check if is connected. May you have not enough permissions?',
                    title="Hey you dumb!")
        elif values[COMMON_ERRORS] == COMMON_ERROR_UNKNOWN:
            sg.popup('Unknown error, calling CIA',
                     title="Hey you dumb!")
        stop_reading()
        stop_writing()

    # Read errors
    elif event == READ_ERROR:
        if values[READ_ERROR] == READ_ERROR_BAD_SUDO:
            sg.popup('Bad password!', 'May is a good idea to use 1234 as password',
                     title="Hey you dumb!")
        else:
            sg.popup(values[READ_ERROR],
                     title="Hey you dumb!")
        stop_reading()
    # Write errors
    elif event == WRITE_ERROR:
        if values[WRITE_ERROR] == WRITE_ERROR_BAD_INPUT:
            sg.popup("Bad input characters",
                     title="Hey you dumb!")
        else:
            sg.popup(values[WRITE_ERROR],
                     title="Hey you dumb!")
        stop_writing()



# Read  values entered by user
event, values = main_window.read()
# access all the values and if selected add them to a string
main_window.close()
